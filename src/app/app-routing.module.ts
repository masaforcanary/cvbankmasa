import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

const appRouting: Routes = [
  { path: '', loadChildren: './layouts/full/full.module#FullModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(appRouting, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

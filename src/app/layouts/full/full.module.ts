import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FullRoutingModule } from './full-routing.module';
import {FullComponent} from './full.component';
import {FooterComponent} from '../../shared/footer/footer.component';
import {MatToolbarModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FullRoutingModule,
    MatToolbarModule
  ],
  declarations: [
    FullComponent,
    FooterComponent
  ]
})
export class FullModule {
}
